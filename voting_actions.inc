<?PHP

/**
 * @file
 * Contains Drupal actions for use with actions.module and the Voting API..
 */


/**
 * Implementation of a Drupal action.
 * Sets the status of a comment to PUBLISHED.
 *
 */
function action_comment_publish($op, $edit = array(), &$comment) {
  switch($op) {
    case 'do':
      $comment->status = COMMENT_PUBLISHED;
      comment_save((array)$comment);
      watchdog('action', t('Set comment id %id to Published', array('%id' => intval($comment->cid))));
      break;

    case 'metadata':
      return array(
        'description' => t('Publish comment'),
        'type' => t('Comment'),
        'batchable' => true,
        'configurable' => false,
      );

    // return an HTML config form for the action
    case 'form':
      return '';

    // validate the HTML form
    case 'validate':
      return TRUE;

    // process the HTML form to store configuration
    case 'submit':
      return '';
  }
}

function action_comment_unpublish($op, $edit = array(), &$comment) {
  switch($op) {
    case 'do':
      $comment->status = COMMENT_NOT_PUBLISHED;
      comment_save((array)$comment);
      watchdog('action', t('Set comment id %id to Unpublished', array('%id' => intval($comment->cid))));
      break;

    case 'metadata':
      return array(
        'description' => t('Unpublish comment'),
        'type' => t('Comment'),
        'batchable' => true,
        'configurable' => false,
      );

    // return an HTML config form for the action
    case 'form':
      return '';

    // validate the HTML form
    case 'validate':
      return TRUE;

    // process the HTML form to store configuration
    case 'submit':
      return '';
  }
}

/**
 * Implementation of a Drupal action.
 * Spawns a comment off as a dedicated node, carrying any reply-comments as child comments.
 * Most certainly and absolutely not yet implemented.
 */
 
/**

function action_promote_comment($op, $edit = array(), &$node) {
  switch($op) {
    case 'metadata':
      return array(
        'description' => t('Promote comment'),
        'type' => t('Comment'),
        'batchable' => false,
        'configurable' => true,
      );

   case 'do':
    // DO STUFF HERE.
    break;

    // return an HTML config form for the action
   case 'form':
    // specify the node-type to use when creating
    // specify behavior:
    //   mirror comment as node
    //   mirror comment and replies
    //   mirror comment and move replies
    //   move comment and replies
    return '';

   // validate the HTML form
   case 'validate':
     return TRUE;

   // process the HTML form to store configuration
   case 'submit':
     return '';
  }
}
 */