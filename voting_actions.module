<?php

/*
 * General drupal functions.
 *
 */

define('VOTING_ACTIONS_NO_MATCH', 0);
define('VOTING_ACTIONS_PASSED', 1);
define('VOTING_ACTIONS_FAILED', 2);

function voting_actions_help($section = "admin/help#voting_actions") {
  switch ($section) {
   }
}

function voting_actions_menu($may_cache) {
  if ($may_cache) {
    $access = user_access('administer voting actions');
    
    $items[] = array(
      'path' => 'admin/settings/voting_actions',
      'title' => t('voting actions'),
      'callback' => 'voting_actions_list',
      'access' => $access,
      'description' => t('Administer voting actions.')
    );
      
    $items[] = array(
      'path' => 'admin/settings/voting_actions/edit',
      'title' => t('edit voting action set'),
      'callback' => 'voting_actions_edit_action',
      'access' => $access,
      'type' => MENU_CALLBACK,
      'description' => t('Edit voting actions.')
    );
      
    $items[] = array(
      'path' => 'admin/settings/voting_actions/delete',
      'title' => t('delete voting action'),
      'callback' => 'voting_actions_delete_action',
      'access' => $access,
      'type' => MENU_CALLBACK,
      'description' => t('Delete voting actions.')
    );
  }
  return $items;
}


/**
 * Implementation of hook_perm().
 */
function voting_actions_perm() {
  return array('administer voting actions');
}


/*
 * Administration pages for the voting_actions data.
 */

function voting_actions_list() {
  $output = '<p>' . t('Each voting action set is a collection of filter criteria, and a collection of actions to be applied when a piece of content is voted on. Create an action set, then edit it to add criteria and actions.') . '</p>';
  $output .= drupal_get_form('voting_actions_form');
  return $output;
}

function voting_actions_form(){
  $content_types = array();
  $result = db_query('SELECT DISTINCT type FROM {actions}');
  while ($content_type = db_fetch_object($result)) {
    $content_types[$content_type->type] = $content_type->type;
  }

  $result = db_query('SELECT va.vasid, va.name, va.content_type, va.weight FROM {voting_actions} va ORDER BY va.weight');

  $form['#tree'] = TRUE; // make sure we preserve all hierarchial values.
  
  while ($va = db_fetch_object($result)) {
    $form['old'][$va->vasid]['name'] = array(
      '#type' => 'textfield',
      '#default_value' => $va->name,
      '#size' => 32,
      '#maxlength' => 128,
    );
    
    $form['old'][$va->vasid]['content_type'] = array(
      '#type' => 'hidden',
      '#value' => $va->content_type,
    );

    $form['old'][$va->vasid]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $va->weight,
      '#delta' => 15,
    );
  }
  
  $form['new']['name'] = array(
    '#type' => 'textfield',
    '#size' => 32,
    '#maxlength' => 128,
  );
  
  $form['new']['content_type'] = array(
    '#type' => 'select',
    '#options' => $content_types,
  );

  $form['new']['weight'] = array(
    '#type' => 'weight',
    '#delta' => 15,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  
  return $form;
}

function voting_actions_form_submit($form_id, $form_values) {
  $sql = "UPDATE voting_actions SET name = '%s', weight = %d WHERE vasid = %d";
  foreach (element_children($form_values['old']) as $vasid) {
    db_query($sql, $form_values['old'][$vasid]['name'], $form_values['old'][$vasid]['weight'], $vasid);
  }

  if ($form_values['new']['name'] && $form_values['new']['content_type']) {
    $sql = "INSERT INTO voting_actions (vasid, name, content_type, weight) VALUES (%d, '%s', '%s', %d)";
    db_query($sql, db_next_id('voting_actions'), $form_values['new']['name'], $form_values['new']['content_type'], $form_values['new']['weight']); 
  }
  drupal_goto('admin/settings/voting_actions');
}

function theme_voting_actions_form(&$form) {
  $header = array(t('action set name'), t('content type'), t('weight'), t('actions'));

  foreach (element_children($form['old']) as $vasid) {
    $row = array();
    $row[] = drupal_render($form['old'][$vasid]['name']);
    $row[] = $form['old'][$vasid]['content_type']['#value'];
    $row[] = drupal_render($form['old'][$vasid]['weight']);
    $row[] = l(t('edit'), 'admin/settings/voting_actions/edit/' . $vasid) . ' | ' . l(t('delete'), 'admin/settings/voting_actions/delete/' . $vasid);
    $rows[] = $row;
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No actions have been defined.'), 'colspan' => '4'));
  }
      
  $row = array();
  $row[] = drupal_render($form['new']['name']);
  $row[] = drupal_render($form['new']['content_type']);
  $row[] = drupal_render($form['new']['weight']);
  $row[] = '';
  $rows[] = $row;

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/*
 * Administration page for editing the individual elements of a voting action.
 */
 
function voting_actions_edit_action($vasid) {
  $output .= drupal_get_form('voting_actions_edit_form', $vasid);
  return $output;
}

function voting_actions_edit_form($vasid){
  $form = array();
  
  $result = db_query('SELECT * FROM {voting_actions} WHERE vasid = %d', $vasid);
  $set = db_fetch_object($result);
  
  $output = '<p><strong>' . $set->name . '</strong>: applies to ' . $set->content_type . ' content.</p>';
  
  $value_types = array(VOTINGAPI_VALUE_TYPE_PERCENT => 'Percentage Scale', VOTINGAPI_VALUE_TYPE_TOKEN => 'Points', VOTINGAPI_VALUE_TYPE_KEY => 'Custom Options'); 
  $comparisons = array('=' => 'is equal to', '<' => 'is less than', '>' => 'is greater than', '!=' => 'does not equal'); 
  
  $available_actions = array();
  $available_actions[] = '--select an action to add--';
  $result = db_query("SELECT * FROM {actions}");
  while ($action = db_fetch_object($result)) {
  	if (strtolower($action->type) == strtolower($set->content_type)) {
  		$available_actions[$action->aid] = $action->description;
  	}
  }
  
  $form['#tree'] = TRUE; // make sure we preserve all hierarchial values.
  $form['set'] = array(
    '#type' => 'hidden',
    '#value' => $set->vasid,
  );
  
  $result = db_query('SELECT * FROM {voting_actions_conditions} WHERE vasid = %d', $vasid);
  while ($criteria = db_fetch_object($result)) {
    $form['criteria']['old'][$criteria->vacid]['value_type'] = array(
      '#type' => 'select',
      '#default_value' => $criteria->value_type,
      '#options' => $value_types,
    );
    
    $form['criteria']['old'][$criteria->vacid]['tag'] = array(
      '#type' => 'textfield',
      '#default_value' => $criteria->tag,
      '#size' => 16,
      '#maxlength' => 64,
    );
    
    $form['criteria']['old'][$criteria->vacid]['function'] = array(
      '#type' => 'textfield',
      '#default_value' => $criteria->function,
      '#size' => 16,
      '#maxlength' => 64,
    );
    
    $form['criteria']['old'][$criteria->vacid]['comparison'] = array(
      '#type' => 'select',
      '#default_value' => $criteria->comparison,
      '#options' => $comparisons,
    );

    $form['criteria']['old'][$criteria->vacid]['value'] = array(
      '#type' => 'textfield',
      '#default_value' => $criteria->value,
      '#size' => 8,
      '#maxlength' => 32,
    );

    $form['criteria']['old'][$criteria->vacid]['delete'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#return_value' => 1,
    );
  }
  
  $form['criteria']['new']['value_type'] = array(
    '#type' => 'select',
    '#options' => $value_types,
  );
  
  $form['criteria']['new']['tag'] = array(
    '#type' => 'textfield',
    '#size' => 16,
    '#maxlength' => 64,
  );
  
  $form['criteria']['new']['function'] = array(
    '#type' => 'textfield',
    '#size' => 16,
    '#maxlength' => 64,
  );
  
  $form['criteria']['new']['comparison'] = array(
    '#type' => 'select',
    '#options' => $comparisons,
  );

  $form['criteria']['new']['value'] = array(
    '#type' => 'textfield',
    '#size' => 8,
    '#maxlength' => 32,
  );

  $result = db_query("SELECT a.aid, a.description FROM {actions} a INNER JOIN {voting_actions_actions} v ON a.aid = v.aid WHERE v.vasid = %d", $vasid);
  while ($action = db_fetch_object($result)) {
    $form['actions']['old'][$action->aid]['aid'] = array(
      '#type' => 'hidden',
      '#value' => $action->description,
    );

    $form['actions']['old'][$action->aid]['delete'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#return_value' => 1,
    );
  }

  $form['actions']['new']['aid'] = array(
    '#type' => 'select',
    '#options' => $available_actions,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  
  return $form;
}


function voting_actions_edit_form_submit($form_id, $form_values) {

  // update any existing criteria records.
  $sql = "UPDATE voting_actions_conditions SET value_type = '%s', tag = '%s', function = '%s', comparison = '%s', value = %d WHERE vacid = %d";
  foreach (element_children($form_values['criteria']['old']) as $vacid) {
    if ($form_values['criteria']['old'][$vacid]['delete']) {
      db_query('DELETE FROM voting_actions_conditions WHERE vacid = %d', $vacid);
    }
    else {
      db_query($sql,
        $form_values['criteria']['old'][$vacid]['value_type'],
        $form_values['criteria']['old'][$vacid]['tag'],
        $form_values['criteria']['old'][$vacid]['function'],
        $form_values['criteria']['old'][$vacid]['comparison'],
        $form_values['criteria']['old'][$vacid]['value'],
        $vacid
      );
    }
  }
  
  // insert the new criteria record if it's been filled out.
  $newrecord = $form_values['criteria']['new'];
  if ($newrecord['value_type'] && $newrecord['function'] && $newrecord['comparison'] && $newrecord['value']) {
    $sql = "INSERT INTO voting_actions_conditions (vacid, vasid, value_type, tag, function, comparison, value) VALUES (%d, %d, '%s', '%s', '%s', '%s', %d)";
    db_query($sql,
      db_next_id('voting_actions_conditions'),
      $form_values['set'],
      $newrecord['value_type'],
      $newrecord['tag'],
      $newrecord['function'],
      $newrecord['comparison'],
      $newrecord['value']
    ); 
  }
  
  // need to process the actual actions now, too.
  // update any existing criteria records.
  foreach (element_children($form_values['actions']['old']) as $aid) {
    if ($form_values['actions']['old'][$aid]['delete']) {
      db_query("DELETE FROM voting_actions_actions WHERE aid = '%s' AND vasid = %d", $aid, $form_values['set']);
    }
  }
  
  // insert the new criteria record if it's been filled out.
  if ($form_values['actions']['new']['aid']) {
    $sql = "INSERT INTO {voting_actions_actions} (aid, weight, vasid) VALUES ('%s', %d, %d)";
    db_query($sql,
      $form_values['actions']['new']['aid'],
      0,
      $form_values['set']
    ); 
  }
  
  _voting_actions_cache_actions();
  drupal_goto('admin/settings/voting_actions/edit/' . $form_values['set']);
}

function theme_voting_actions_edit_form(&$form) {
  $output .= '<h3>' . t('Filter criteria') . '</h3>';
  $output .= '<p>' . t('Incoming vote results will be compared against these criteria.') . '</p>';

  $header = array(t('value type'), t('tag'), t('function'), t('comparison'), t('value'), t('delete'));

  foreach (element_children($form['criteria']['old']) as $vacid) {
    $row = array();
    $row[] = drupal_render($form['criteria']['old'][$vacid]['value_type']);
    $row[] = drupal_render($form['criteria']['old'][$vacid]['tag']);
    $row[] = drupal_render($form['criteria']['old'][$vacid]['function']);
    $row[] = drupal_render($form['criteria']['old'][$vacid]['comparison']);
    $row[] = drupal_render($form['criteria']['old'][$vacid]['value']);
    $row[] = drupal_render($form['criteria']['old'][$vacid]['delete']);
    $rows[] = $row;
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No filter criteria have been defined.'), 'colspan' => '6'));
  }
  
  $row = array();
  $row[] = drupal_render($form['criteria']['new']['value_type']);
  $row[] = drupal_render($form['criteria']['new']['tag']);
  $row[] = drupal_render($form['criteria']['new']['function']);
  $row[] = drupal_render($form['criteria']['new']['comparison']);
  $row[] = drupal_render($form['criteria']['new']['value']);
  $row[] = '';
  $rows[] = $row;

  $output .= theme('table', $header, $rows);

  $output .= '<h3>' . t('Actions') . '</h3>';
  $output .= '<p>' . t('These actions will be performed on content if the incoming vote results match all of the above critera.') . '</p>';

  $header = array(t('action'), t('delete'));

  unset($rows);
  foreach (element_children($form['actions']['old']) as $aid) {
    $row = array();
    $row[] = $form['actions']['old'][$aid]['aid']['#value'];
    $row[] = drupal_render($form['actions']['old'][$aid]['delete']);
    $rows[] = $row;
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No actions have been defined.'), 'colspan' => '6'));
  }
  
  $row = array();
  $row[] = drupal_render($form['actions']['new']['aid']);
  $row[] = '';
  $rows[] = $row;

  $output .= theme('table', $header, $rows);

  $output .= drupal_render($form);

  return $output;
}


/*
 * Deleting a given voting action.
 */

function voting_actions_delete_action($vasid) {
  return drupal_get_form('voting_actions_delete_action_confirm_form', arg(1), $vasid);
}

function voting_actions_delete_action_confirm_form($nid, $vasid){
  $form['vasid'] = array(
    '#type' => 'hidden',
    '#value' => $vasid,
  );

  $output = confirm_form(
    $form,
    t('Are you sure you want to delete this voting action set?'),
    $_GET['destination'] ? $_GET['destination'] : 'admin/settings/voting_actions',
    t('This action cannot be undone.'),
    t('Delete Set'),
    t('Cancel') 
  );
  return $output;
}

function voting_actions_delete_action_confirm_form_submit($form_id, $form_values) {
  if ($form_values['confirm']) {
    if (user_access('administer voting actions')) {
      $vasid = $form_values['vasid'];

      db_query('DELETE FROM {voting_actions_actions} WHERE vasid = %d', $vasid);
      db_query('DELETE FROM {voting_actions_conditions} WHERE vasid = %d', $vasid);
      db_query('DELETE FROM {voting_actions} WHERE vasid = %d', $vasid);
      
      drupal_set_message(t('Set deleted.'));
      drupal_goto('admin/settings/voting_actions');
    }
    else {
      drupal_access_denied();
    }
  }
}



/*
 * The code that actual does stuff.
 * The votingapi hook implementation handles incoming 'post calculation' events
 * and acts on the results.
 *
 */

function voting_actions_votingapi_results($cache, $votes, $content_type, $content_id) {
  $action_set = variable_get('voting_actions', NULL);
  if ($action_set == NULL) {
    $action_set = _voting_actions_cache_actions();
  }
  // loop through all the actions, check against the conditions, and execute the actions if they pass.
  foreach ($action_set as $vote_action) {
    $check = VOTING_ACTIONS_NO_MATCH;
    if (strtolower($vote_action->content_type) == strtolower($content_type)) {
  	  foreach ($cache as $cached_result) {
        if ($check == VOTING_ACTIONS_FAILED) {
          break;
        }
        if (is_array($vote_action->conditions)) {
          foreach ($vote_action->conditions as $condition) {
            if ($check == VOTING_ACTIONS_FAILED) {
              break;
            }
            
            switch (voting_actions_check_result_against_condition($cached_result, $condition)) {
              case VOTING_ACTIONS_NO_MATCH:
                break;
              case VOTING_ACTIONS_PASSED:
                $check = VOTING_ACTIONS_PASSED;
                break;
              case VOTING_ACTIONS_FAILED:
                $check = VOTING_ACTIONS_FAILED;
                break;
            }
          }
        }
      }
      
      if ($check == VOTING_ACTIONS_PASSED) {
        // if all conditions are met, execute all the actions.
        // for now, it only really works with nodes and comments.
        switch ($content_type) {
          case 'node':
          $content = node_load($content_id);
          break;
          
          case 'comment':
          $content = _comment_load($content_id);
          break;
        }
        
        actions_do($vote_action->actions, $content);
      }
    }
    }
}

function voting_actions_check_result_against_condition($result, $condition) {
  if ($condition->value_type != $result->value_type) {
    return VOTING_ACTIONS_NO_MATCH;
  }
  if (strtolower($condition->function) != strtolower($result->function)) {
    return VOTING_ACTIONS_NO_MATCH;
  }
  if ($condition->tag != '' && strtolower($condition->tag) != strtolower($result->tag)) {
    return VOTING_ACTIONS_NO_MATCH;
  }
  switch ($condition->comparison) {
    case '<':
      if ($result->value >= $condition->value) return VOTING_ACTIONS_FAILED;
      break;
  
    case '=':
      if ($result->value != $condition->value) return VOTING_ACTIONS_FAILED;
      break;
  
    case '!=':
      if ($result->value == $condition->value) return VOTING_ACTIONS_FAILED;
      break;
  
    case '>':
      if ($result->value <= $condition->value) return VOTING_ACTIONS_FAILED;
      break;
  }
  return VOTING_ACTIONS_PASSED;
}

function _voting_actions_cache_actions() {
  $action_set = array();
  // cache a list of the possible vote_actions, including the content types they apply to (node, comment, etc)
  $sql = "SELECT vasid, name, content_type, weight FROM voting_actions ORDER BY weight";
  $result = db_query($sql);
  while ($va = db_fetch_object($result)) {
    $action_set[$va->vasid] = $va;
  }
  
  // Cache a list of conditions for each action.
  $sql = "SELECT vacid, vasid, value_type, tag, function, comparison, value FROM voting_actions_conditions";
  $result = db_query($sql);
  while ($vac = db_fetch_object($result)) {
    $action_set[$vac->vasid]->conditions[] = $vac;
  }
    
  // Cache a list of actions for each action.
  $sql = "SELECT aid, vasid FROM voting_actions_actions";
  $result = db_query($sql);
  while ($vaa = db_fetch_object($result)) {
    $action_set[$vaa->vasid]->actions[] = $vaa->aid;
  }
  
  variable_set('voting_actions', $action_set);
  return $action_set;
}
